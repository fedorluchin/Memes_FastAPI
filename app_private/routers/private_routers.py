from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Form
from sqlalchemy.orm import Session
from .. import models, schemas
from app_private.db import get_db
from media_service import media, security_logic

priv_router = APIRouter()


@priv_router.post("/api/v1/memes", response_model=schemas.Meme)
def create_meme(image_text: str = Form(...),
                image: UploadFile = File(...),
                db: Session = Depends(get_db),
                username: str = Depends(security_logic.get_current_username)
                ):
    meme_data = schemas.MemeBase(text=image_text).dict()
    image_url = media.upload_image(image)  # Загрузка изображения в MinIO и получение URL
    meme_data['image_url'] = image_url  # Добавление URL изображения к данным мема
    db_meme = models.Meme(**meme_data)
    db.add(db_meme)
    db.commit()
    db.refresh(db_meme)
    return db_meme


@priv_router.put("/api/v1/memes/{id}", response_model=schemas.Meme)
def update_meme(id: int,
                meme: schemas.MemeCreate,
                db: Session = Depends(get_db),
                username: str = Depends(security_logic.get_current_username)
                ):
    db_meme = db.query(models.Meme).filter(models.Meme.id == id).first()
    if db_meme is None:
        raise HTTPException(status_code=404, detail="Meme not found")
    db_meme.text = meme.text
    db_meme.image_url = meme.image_url
    db.commit()
    db.refresh(db_meme)
    return db_meme


@priv_router.delete("/api/v1/memes/{id}", response_model=schemas.Meme)
def delete_meme(id: int,
                db: Session = Depends(get_db),
                username: str = Depends(security_logic.get_current_username)
                ):
    db_meme = db.query(models.Meme).filter(models.Meme.id == id).first()
    if db_meme is None:
        raise HTTPException(status_code=404, detail="Meme not found")
    file_name = db_meme.image_url.split('&prefix=')[-1]
    media.delete_image(file_name)
    db.delete(db_meme)
    db.commit()
    return db_meme

