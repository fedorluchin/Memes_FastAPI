from typing import Optional

from pydantic import BaseModel


class MemeBase(BaseModel):
    text: str


class MemeCreate(MemeBase):
    image_url: Optional[str] = None


class Meme(MemeBase):
    id: int
    image_url: Optional[str] = None

    class Config:
        from_attributes = True
