from fastapi import FastAPI
from app_private.routers import private_routers
from app_private.db import create_db


create_db()

app_private = FastAPI()

app_private.include_router(private_routers.priv_router)


