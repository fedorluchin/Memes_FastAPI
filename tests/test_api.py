import requests
from requests.auth import HTTPBasicAuth

LOGIN_CREDS = "admin"
PASS_CREDS = "admin"
HOST_API = "127.0.0.1"
PORT_API_PUB = "8001"
PORT_API_PRIVATE = "8002"
API_VERSION = "api/v1"


def test_create_meme_protected():
    url = f"http://{HOST_API}:{PORT_API_PRIVATE}/{API_VERSION}/memes"
    auth = HTTPBasicAuth(LOGIN_CREDS, PASS_CREDS)

    image_text = 'TestText'
    username = 'admin'
    files = {
        'image': ('test.jpg', b'test_bytes', 'image/jpeg'),
    }
    data = {
        "image_text": image_text,
        "username": username
    }
    response = requests.post(url, auth=auth, data=data, files=files)
    assert response.status_code == 200
    assert 'text' in response.json()
    assert 'image_url' in response.json()


def test_read_memes():
    url = f"http://{HOST_API}:{PORT_API_PUB}/{API_VERSION}/memes"
    params = {'skip': 0, 'limit': 10}
    response = requests.get(url, params=params)

    assert response.status_code == 200
    memes = response.json()
    assert isinstance(memes, list)
    assert len(memes) <= 10
    for meme in memes:
        assert 'id' in meme
        assert 'image_url' in meme
        assert 'text' in meme


# from fastapi.testclient import TestClient
# from app_public.main import app_public
# from fastapi import UploadFile
# from io import BytesIO
#
# client = TestClient(app_public)
#
#
# def test_create_meme():
#     image_text = 'MyText'
#     image = UploadFile(
#         filename='test.jpg',
#         file=BytesIO(b'my file contents'),
#     )
#     username = 'admin'
#     # Выполняем POST запрос
#     response = client.post(
#         "/memes",
#         data={"image_text": image_text, "username": username},
#         files={"image": image}
#     )
#     # Проверяем статус ответа
#     assert response.status_code == 200
#     assert 'text' in response.json()
#     assert 'image_url' in response.json()


