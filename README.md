# Memes API

![PyPI - Version](https://img.shields.io/pypi/v/fastapi)
![PyPI - License](https://img.shields.io/pypi/l/fastapi)
![GitLab Stars](https://img.shields.io/gitlab/stars/fedorluchin%2Fpublic_portfolio)
![GitLab Forks](https://img.shields.io/gitlab/forks/fedorluchin%2Fpublic_portfolio)

**Memes API** - это веб-приложение для работы с коллекцией мемов. Оно состоит из базы данных PostgreSQL, S3-хранилища MinIO, 
и двух сервисов на FastAPI для приватного и публичного API.

## Описание функционала
### 1. Сервис с публичным API
Просмотр списка мемов с пагинацией и определенного мема по его id:
  - GET /api/v1/memes: Получить список всех мемов (с пагинацией).
  - GET /api/v1/memes/{id}: Получить конкретный мем по его ID.

### 2. Сервис с приватным API
Загрузка мема с текстом и изображением, удаление и редактирование мема. API защищено от постороннего доступа с помощью авторизации.
Креды находятся в [файле env.example](.env.example):
  - POST /api/v1/memes: Добавить новый мем (с картинкой и текстом).
  - PUT /api/v1/memes/{id}: Обновить существующий мем.
  - DELETE /api/v1/memes/{id}: Удалить мем.


## Локальный запуск
Поместите данные из [файла env.example](.env.example) в [файл env](.env).

Для запуска веб-приложения с помощью Docker используйте команды:
```
docker-compose build
docker-compose up
```

## Документация API
После запуска приложения документация Swagger UI будет доступна по адресу:
```
http://localhost:8001/docs
http://localhost:8002/docs
```
MinIO UI располагается по адресу:
```
http://localhost:9001/browser
```

## Тестирование
Для запуска тестов используйте в терминале в корне проекта команду:
```
pytest
```
Или вызовите файл напрямую с помощью команды:
```
python -m tests.test_api
```