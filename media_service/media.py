from fastapi import UploadFile
from minio import Minio
from minio.error import S3Error

MINIO_HOSTNAME = "s3-storage:9000"
MINIO_BUCKET = "memes"

minio_client = Minio(
    MINIO_HOSTNAME,
    access_key="minio",
    secret_key="minio123",
    secure=False
)


def upload_image(file: UploadFile):
    try:
        result = minio_client.put_object(
            bucket_name=MINIO_BUCKET,
            object_name=file.filename,
            data=file.file,
            content_type=file.content_type,
            length=-1,
            part_size=100 * 1024 * 1024,
        )
        return f"localhost:9001/api/v1/buckets/{result.bucket_name}/objects/download?preview=true&prefix={file.filename}"
    except S3Error as exc:
        print(f"Ошибка при загрузке файла: {exc}")


def delete_image(file_name: str):
    try:
        minio_client.remove_object(MINIO_BUCKET, file_name)
    except S3Error as exc:
        print(f"Ошибка при удалении файла: {exc}")