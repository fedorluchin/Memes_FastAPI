from fastapi import FastAPI
from app_public.routers import public_routers
from app_public.db import create_db


create_db()

app_public = FastAPI()

app_public.include_router(public_routers.pub_router)

