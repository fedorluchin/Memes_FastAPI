from sqlalchemy import Column, Integer, String, create_engine
from app_public.db import Base
# from sqlalchemy.orm import declarative_base


class Meme(Base):
    __tablename__ = 'memes'

    id = Column(Integer, primary_key=True, index=True)
    image_url = Column(String, unique=True, index=True)
    text = Column(String, index=True)

