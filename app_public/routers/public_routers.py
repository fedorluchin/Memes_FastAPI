from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from .. import models, schemas
from app_public.db import get_db

pub_router = APIRouter()


@pub_router.get("/api/v1/memes", response_model=list[schemas.Meme])
def read_memes(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    memes = db.query(models.Meme).offset(skip).limit(limit).all()
    return memes


@pub_router.get("/api/v1/memes/{id}", response_model=schemas.Meme)
def read_meme(id: int, db: Session = Depends(get_db)):
    meme = db.query(models.Meme).filter(models.Meme.id == id).first()
    if meme is None:
        raise HTTPException(status_code=404, detail="Meme not found")
    return meme


